#ifndef BMICALCULATOR_H
#define BMICALCULATOR_H

#include <QtCore>
#include <QString>

enum Units {kg_cm, lb_in};

class BMICalculator
{
public:
    BMICalculator(double weight, double height, Units units);
    void calculateBMI();
    void findCategory();
    double getBmi() const;
    QString getCategory() const;

private:
    double weight;
    double height;
    Units units;
    double bmi;
    QString category;
};

#endif // BMICALCULATOR_H
