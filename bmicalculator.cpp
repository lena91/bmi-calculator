#include "bmicalculator.h"

BMICalculator::BMICalculator(double weight, double height, Units units)
{
    this->weight = weight;
    this->height = height;
    this->units = units;
    calculateBMI();
    findCategory();
}

void BMICalculator::calculateBMI()
{
    if (units == kg_cm)
        bmi = (static_cast<double>(weight) / ((static_cast<double>(height) * static_cast<double>(height)) / 10000));
    else
        bmi = (static_cast<double>(weight) / (static_cast<double>(weight) * static_cast<double>(weight))) * 703;
}

void BMICalculator::findCategory()
{
    if (bmi < 15)
        category = "Very severely underweight";
    else if (bmi < 16)
        category = "Severely underweight";
    else if (bmi < 18.5)
        category = "Underweight";
    else if (bmi < 25)
        category = "Normal (healthy weight)";
    else if (bmi < 30)
        category = "Overweight";
    else if (bmi < 35)
        category = "Obese Class I (Moderately obese)";
    else if (bmi < 40)
        category = "Obese Class II (Severely obese)";
    else if (bmi < 45)
        category = "Obese Class III (Very severely obese)";
    else if (bmi < 50)
        category = "Obese Class IV (Morbidly Obese)";
    else if (bmi < 60)
        category = "Obese Class V (Super Obese)";
    else
        category = "Obese Class VI (Hyper Obese)";

}

double BMICalculator::getBmi() const
{
    return bmi;
}

QString BMICalculator::getCategory() const
{
    return category;
}
