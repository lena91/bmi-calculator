#ifndef BMIMAINWINDOW_H
#define BMIMAINWINDOW_H

#include <QMainWindow>
#include "bmicalculator.h"

namespace Ui {
class BMIMainWindow;
}

class BMIMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit BMIMainWindow(QWidget *parent = nullptr);
    ~BMIMainWindow();

private slots:
    void on_calculatePushButton_clicked();

private:
    Ui::BMIMainWindow *ui;
};

#endif // BMIMAINWINDOW_H
