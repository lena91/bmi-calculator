#include "bmimainwindow.h"
#include "ui_bmimainwindow.h"

BMIMainWindow::BMIMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::BMIMainWindow)
{
    ui->setupUi(this);
}

BMIMainWindow::~BMIMainWindow()
{
    delete ui;
}

void BMIMainWindow::on_calculatePushButton_clicked()
{
    bool isValidWeight = false;
    bool isValidHeight = false;
    double weight = ui->weightLineEdit->text().toDouble(&isValidWeight);
    double height = ui->heightLineEdit->text().toDouble(&isValidHeight);
    Units units = ui->kgmRadioButton->isChecked() ? kg_cm : lb_in;

    if (isValidWeight && isValidHeight)
    {
        BMICalculator bmiCalculator(weight, height, units);
        ui->bmiResultLabel->setText(QString::number(bmiCalculator.getBmi(), 'g', 4));
        ui->bmiCategoryLabel->setText(bmiCalculator.getCategory());
    }
    else
    {
        ui->bmiResultLabel->clear();
        ui->bmiCategoryLabel->clear();
    }
}
